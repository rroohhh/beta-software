#!/bin/bash
echo "starting the full build..."
mkdir -p build
cd build
set -e 
set -o pipefail

echo -e "\ninstalling requirements:\n"
apt-get update -qq
apt-get install -y -qq $(grep -vE "^\s*#" ../build_tools/outside/dependencies.txt | tr "\n" " ") > /dev/null


echo -e "\nbuilding the image:\n"
../build_tools/outside/build_image.sh


echo -e "\nbuilding qemu:\n"
../build_tools/outside/build_qemu.sh


echo -e "\ncustomizing the image & doing the inside build:\n"
../build_tools/outside/run_qemu.expect


echo "build finished :)"