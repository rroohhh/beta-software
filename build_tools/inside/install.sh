#!/bin/bash
set -e 
set -o pipefail
cd /opt/beta-software

# install dependencies
pacman -Syu --noconfirm
pacman --noconfirm -S $(grep -vE "^\s*#" build_tools/inside/dependencies.txt | tr "\n" " ")

# setup hostname and add user
echo "axiom-beta" > /etc/hostname

PASS=axiom
USERNAME=apertus
if ! getent passwd $USERNAME > /dev/null 2>&1; then
    useradd -d /home/"$USERNAME" -m -g users -s /bin/bash "$USERNAME"
    echo "generating new random password:"
    mkpasswd "$USERNAME"
fi

# Add login headers to users
figlet "Axiom-Beta" > /etc/motd
echo "Software version $(git rev-parse --short HEAD). Last updated on $(date +"%d.%m.%y %H:%M UTC")" >> /etc/motd
echo "To update run, \"axiom-update\"." >> /etc/motd


# build all the tools
cd /opt/beta-software
for dir in software/cmv_tools/* software/processing_tools/*; do 
    if [[ -d $dir ]]; then
    	cd $dir
    	make 
    	make install
    	cd /opt/beta-software
    fi
done

for script in software/scripts/*.sh software/scripts/*.py; do
    ln -sfn $script /usr/local/bin/axiom-$(basename $script .sh)
done

ln -sfn build_tools/inside/install.sh /usr/local/bin/axiom-update

# TODO: build and install the control daemon
